/**
 * This file contains all the constants.
 * NOTE: to change the variables, on the run-time, remove the readonly tag, and re-generate positions.
 */

namespace monoloco.core {
    export class Constants {
        public static readonly ROW_LIM: number = 8;
        public static readonly COL_LIM: number = 10;
        // number of reserved positions.
        public static readonly PLAYER_SEL_NUM: number = 10;
        public static readonly FISH_SHAPE: number[][][] =
            [
                [[0, 0], [0, 1], [1, 0], [1, 1]],
                [[0, 0], [0, 1], [1, 0], [1, 1], [1, 2]],
                [[0, 0], [0, 1], [1, 0], [1, 1], [0, 2], [1, 2]]
            ];
        public static readonly DEFAULT_BG_COLOR: string = "#D3D3D3";
        public static readonly COLOR_ARRAY: string[] = [
            "#00FFFF",
            "#FF0000",
            "#00FF00",
            "#0000FF",
            "#FFFF00",
            "#8B008B"
        ]
    }
}
