namespace monoloco.utils {
    export function shuffle(arr: Array<any>): Array<any> {
        for (let rightPos: number = arr.length - 1; rightPos >= 1; rightPos--) {
            let randPos: number = Math.floor(Math.random() * rightPos);
            // swap arr[rightPos] and arr[randPos]
            [arr[randPos], arr[rightPos]] = [arr[rightPos], arr[randPos]];
        }
        return arr;
    };
}
