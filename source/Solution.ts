/**
 * This class contains our solution.
 * Call startPicking() from console to change the positions.
 */

namespace monoloco.core {
    export class Solution {
        private playerPicks: Array<number> = [];
        private nonPlayerPicks: number[] = [];
        /** All eligible positions for all types of Fishes. */
        private allEligible: number[][] = [];
        private currentTestPos: number[] = [];
        private finalFishPos: number[][] = [];
        /** Flag to tell whether the final positions has been finalized. */
        private done: boolean = false;

        public constructor() {
            this.startPicking();
        }

        /**
         * Initializes a new picking process.
         * Reset all previously set variables, and start picking process from fresh.
         */
        public startPicking(): void {
            this.reset();
            this.randomPlayerPick();
            // this.writeTable(this.playerPicks, this.nonPlayerPicks);
            this.storeAllEligiblePos();
            this.assignFishPos();
            this.formatShapes();
            this.writeTable(this.playerPicks, this.finalFishPos[0], this.finalFishPos[1], this.finalFishPos[2]);
        }

        private reset(): void {
            this.playerPicks = [];
            this.nonPlayerPicks = [];
            this.allEligible = [];
            this.currentTestPos = [];
            this.finalFishPos = [];
            this.done = false;
        }

        /**
         * Ramdomly pick some positions as player selected positions.
         * @param pickLim The number of player positions to select
         */
        public randomPlayerPick(pickLim: number = Constants.PLAYER_SEL_NUM): number[] {
            let allPos: number[] = [];
            for (let row: number = 0; row < Constants.ROW_LIM; row++) {
                for (let col: number = 0; col < Constants.COL_LIM; col++) {
                    allPos.push(row * Constants.COL_LIM + col);
                }
            }

            utils.shuffle(allPos);
            this.playerPicks = allPos.slice(0, pickLim);
            this.nonPlayerPicks = allPos.slice(pickLim);
            return this.playerPicks.concat();
        }

        /**
         * Create an HTML table of cells with different background colours.
         * @param args: array of positions to display. 
         * It shows 80 grids, and colours the positions in the array.  
         */
        public writeTable(...args: number[][]): void {
            let html: string = "<table>";
            for (let row: number = 0; row < Constants.ROW_LIM; row++) {
                html += "<tr>";
                for (let col: number = 0; col < Constants.COL_LIM; col++) {
                    let pos: number = row * Constants.COL_LIM + col;
                    let posInArgs: boolean = false;
                    args.forEach((posType: number[], idx: number) => {
                        if (posType.indexOf(pos) !== -1 && !posInArgs) {
                            posInArgs = true;
                            html += "<td bgcolor='" + Constants.COLOR_ARRAY[idx] + "'>" + "</td>";
                        }
                    });
                    if (!posInArgs) {
                        html += "<td bgcolor='" + Constants.DEFAULT_BG_COLOR + "'>" + "</td>";
                    }
                }
                html += "</tr>";
            }
            html += "</table>";
            tableDiv.innerHTML = html;
        }

        /**
         * Creates and stores a 2D array which contains all the eligible positions for all types of fish.
         */
        private storeAllEligiblePos(): void {
            Constants.FISH_SHAPE.forEach((type: number[][], index: number) => {
                this.allEligible[index] = [];
            });
            this.nonPlayerPicks.forEach((pos: number) => {
                // let posX: number = Math.floor(pos / Constants.COL_LIM);
                // let posY: number = pos % Constants.COL_LIM;

                Constants.FISH_SHAPE.forEach((fishType: number[][], index: number) => {
                    if (this.isEligible(pos, fishType)) {
                        this.allEligible[index].push(pos);
                    }
                });
            });
            // shuffle eligible positions for all fishes again to appear more random
            this.allEligible.forEach((eligiblePosArray: number[]) => {
                utils.shuffle(eligiblePosArray);
            });
        }

        /**
         * Determines if a given positions is eligible for certain type of fish.
         * @param posX 
         * @param posY 
         * @param fishOffsets 
         * @returns {boolean} whether the position is eligible.
         */
        private isEligible(pos: number, fishOffsets: number[][]): boolean {
            for (let i: number = 0; i < fishOffsets.length; i++) {
                let newRow: number = Math.floor(pos / Constants.COL_LIM) + fishOffsets[i][0];
                if (newRow >= Constants.ROW_LIM) {
                    return false;
                }
                let col: number = (pos % Constants.COL_LIM) + fishOffsets[i][1];
                if (col >= Constants.COL_LIM) {
                    return false;
                }
                let newPos: number = pos + fishOffsets[i][0] * Constants.COL_LIM + fishOffsets[i][1];
                if (this.playerPicks.indexOf(newPos) !== -1) {
                    return false;
                }
            }
            return true;
        }

        /**
         * Assign positions for all fish types.
         * Start with any one fish type, assign a test position, continue with next fish, 
         * and backtrack if not possible.
         * Once all the test positions are verified, we're done.
         * @param fishTypeIndex 
         */
        private assignFishPos(fishTypeIndex: number = this.allEligible.length - 1): void {
            if (fishTypeIndex < 0) {
                this.done = true;
                return;
            }
            for (let index: number = 0; index < this.allEligible[fishTypeIndex].length; index++) {
                if (this.done) {
                    return;
                }
                if (this.checkCompatible(this.allEligible[fishTypeIndex][index], Constants.FISH_SHAPE[fishTypeIndex])) {
                    this.currentTestPos[fishTypeIndex] = this.allEligible[fishTypeIndex][index];
                    this.assignFishPos(fishTypeIndex - 1);
                }
            }
        }

        /**
         * Check whether the give position is compatible with all the previously set currentTestPos
         * @param pos 
         */
        private checkCompatible(pos: number, shape: number[][]): boolean {
            for (let index: number = 0; index < this.currentTestPos.length; index++) {
                if (this.currentTestPos[index] === undefined || this.currentTestPos[index] === null) {
                    continue;
                }
                if (this.isOverlapping(pos, shape, this.currentTestPos[index], Constants.FISH_SHAPE[index])) {
                    return false;
                }
            }
            return true;
        }

        /**
         * This auxiliary fuction takes the test positions fir all fishes and generate boxes according to their shape. 
         */
        private formatShapes(): void {
            if (!this.done) { return; }

            Constants.FISH_SHAPE.forEach((type: number[][], index: number) => {
                this.finalFishPos[index] = [];
            });

            Constants.FISH_SHAPE.forEach((type: number[][], index: number) => {
                type.forEach((offset: number[], index2: number) => {
                    let newPos: number = this.currentTestPos[index] + offset[0] * Constants.COL_LIM + offset[1];
                    this.finalFishPos[index].push(newPos);
                });
            });
        }

        /**
         * Whether two shapes are overlapping.
         * @param pos1 starting position of first shape.
         * @param shape1 the shape array for first shape.
         * @param pos2 starting position of second shape.
         * @param shape2 the shape array for second shape.
         * @returns {boolean} 
         */
        private isOverlapping(pos1: number, shape1: number[][], pos2: number, shape2: number[][]): boolean {
            let box1: number[] = [];
            let box2: number[] = [];

            for (let i: number = 0; i < shape1.length; i++) {
                let tempPos: number = pos1 + shape1[i][0] * Constants.COL_LIM + shape1[i][1];
                box1.push(tempPos);
            }
            for (let j: number = 0; j < shape2.length; j++) {
                let tempPos: number = pos2 + shape2[j][0] * Constants.COL_LIM + shape2[j][1];
                box2.push(tempPos);
            }

            // if the intersection is non-empty
            if (box1.filter(value => -1 !== box2.indexOf(value)).length) {
                return true;
            } else {
                return false;
            }
        }
    }
}
