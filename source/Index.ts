/// <reference path="Interfaces.ts" />

/**
 * This file is the entry point of this project.
 * Declare all initial variables here, instantiate classes, and call relevant functions.
 */
namespace monoloco.core {
    export let tableDiv: HTMLElement = document.getElementById("outputTable") as HTMLElement;
    export let solution: Solution = new Solution();
}
