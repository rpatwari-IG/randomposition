# Find Solution

## About
    This is a testing page for trying an algorithm.
    Problem: Given a grid of size ROW_LIM x COL_LIM, generate PLAYER_SEL_NUM number of reserved positions. Out of the remaining reserved positions, generate some shapes that are non-overlapping with each others.
    In other words, generate some mutually exckusive shapes out of unreserved positions in a grid.

## Project Setup
1. Clone repository at local directory. Go to the root directory
2. Open cmd and run ```http-server```.
3. Open the provided local server link in browser.

## Author
    Rahul Kumar Patwari

## Date
    7th April 2019, 8:00 PM (IST).
